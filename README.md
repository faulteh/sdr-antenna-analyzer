# sdr-antenna-analyzer

## About

I'm trying to learn about DSP and programming GNURadio blocks, so this
repo has a (simple?) project to make an antenna analyzer using GNURadio that
can sweep the entire band of a SDR and produce a plot of VSWR. It can also
be used to test a filter circuit.

The SDR needs to be full duplex and have separate connectors for both TX and
RX. Eg USRP, LimeSDR.

You will need a VSWR bridge that can handle the frequencies you want to
scan. You can find these on EBay.

## Using

Use the Jupyter notebook. The GRC files are just working examples of looking at data from a specific frequency.

### Calibration

Plug in a 50 ohm load and run a calibration sweep. Then plug in your device
under test and run a scan. If calibration data exists it will subtract from
your scan result.

## Author

Scott Bragg (jsbragg@scriptforge.org)
